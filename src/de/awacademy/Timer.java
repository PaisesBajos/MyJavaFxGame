package de.awacademy;

import de.awacademy.model.Model;
import de.awacademy.model.Sprite;
import javafx.animation.AnimationTimer;


public class Timer extends AnimationTimer {

    private Graphics graphics;
    KeyEvents myEvents;
    private Model myModel;
    IntersectionTest test;


    public Timer(Graphics graphics, KeyEvents myEvents, Model myModel) {
        this.graphics = graphics;
        this.myEvents = myEvents;
        this.myModel = myModel;
        test = new IntersectionTest();
    }

    @Override
    public void handle(long nowNano) {
        //Bool for switching between program states
        if (myEvents.getBool()) {
            graphics.setState(2);
            myModel.update();
            testIntersectionsUweTrash();
            testIntersectionsUweTeflon();
            testUweLeavingArea();
            if (myEvents.getKeyEvents().contains("SPACE")) {
                myModel.getUwe().updateSpace();
            }
        }
        graphics.draw();
    }

    private void testIntersectionsUweTrash() {
        if (test.intersects(myModel.getUwe(), myModel.getSpawny().getMySprites())) {
            myModel.getUwe().deminishLifes();

            if (myModel.getUwe().getLifes() == 0) {

                graphics.setState(3);
                stop();

            }
        }
    }

    private void testIntersectionsUweTeflon() {
        if (test.intersects(myModel.getUwe(), myModel.getTSpawner().getMyTeflon())) {
            myModel.getUwe().increaseLifes();
            myModel.getTSpawner().removeLatest();
            //myModel.getTSpawner().removeLatest();
            if (myModel.getUwe().getLifes() > 3) {
                myModel.getUwe().setLifes(3);
            }
        }
    }


    private void testUweLeavingArea() {
        double minX = myModel.getUwe().getPositionX();
        double maxX = minX + myModel.getUwe().getWidth();
        double minY = myModel.getUwe().getPositionY();
        double maxY = minY + myModel.getUwe().getHeight();

        if (minX < Sprite.WIDTH / 12 || maxX > 11 * Sprite.WIDTH / 12 || minY < Sprite.HEIGHT / 12 || maxY > 11 * Sprite.HEIGHT / 12) {
            graphics.setState(3);
            stop();///Exit Condition
        }

    }
}
