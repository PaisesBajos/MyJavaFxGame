package de.awacademy;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class Sound {

    String file;
    Media sound;
    MediaPlayer musicPlayer;

    public Sound(String file) {
        this.file = file;
        this.sound = new Media(new File(file).toURI().toString());
        this.musicPlayer = new MediaPlayer(sound);
    }

    public void play(){
        musicPlayer.play();
    }

    public String getFile() {
        return file;
    }

    public MediaPlayer getMusicPlayer() {
        return musicPlayer;
    }

    public void setVolume(double value){
        musicPlayer.setVolume(value);
    }

    public double getVolume(){
        return musicPlayer.getVolume();
    }
}

