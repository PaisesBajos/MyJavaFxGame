package de.awacademy.model;

public class ThrowingSpriteLeft extends Sprite {
    protected double angle= 0;
    public ThrowingSpriteLeft(Sprite uwe){
        super();
        setPosition(WIDTH-(9.0/10.0)*WIDTH, HEIGHT -(1.0/10.0)* HEIGHT);
        setVelocity(-uwe.getPositionX()*0.8/100,(WIDTH-0.8*uwe.getPositionY())/120);
        this.setHeight(30*0.8);
        this.setWidth(30*0.8);
    }
    public void update() {

        //Gravity
        setVelocityY(getVelocityY() - 0.1);
        setPositionY(getPositionY() - getVelocityY());
        setPositionX(getPositionX()-getVelocityX());
        ;
    }

    @Override
    public double calculateAngle() {
        angle+=1 ;
        return angle;
    }

}
