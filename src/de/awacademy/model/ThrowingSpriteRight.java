package de.awacademy.model;

public class ThrowingSpriteRight extends ThrowingSpriteLeft {
    public ThrowingSpriteRight(Sprite uwe) {
        super(uwe);
        setPosition(WIDTH - (1.5 / 10.0) * WIDTH, HEIGHT - (1 / 10.0) * HEIGHT);
        setVelocity((WIDTH-uwe.getPositionX()) * 0.8 / 100, (WIDTH - 0.8 * uwe.getPositionY()) / 120);

    }

    @Override
    public double calculateAngle() {
        angle -= 1;
        return angle;
    }

}
