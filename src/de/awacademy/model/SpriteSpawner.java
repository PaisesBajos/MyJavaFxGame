package de.awacademy.model;


import java.util.LinkedList;
import java.util.Queue;

public class SpriteSpawner {
    Queue<Sprite> mySprites;
    Uwe uwe;
    private long tick = 0;
    boolean alternatingLeftAndRight = true;

    public SpriteSpawner(Uwe uwe) {
        this.mySprites = new LinkedList<Sprite>();
        this.uwe = uwe;
    }

    public void update() {
       int faster = 1;
        if (tick % 120 == 0) {
            if (alternatingLeftAndRight) {
                mySprites.add(new ThrowingSpriteLeft(uwe));
            } else {
                mySprites.add(new ThrowingSpriteRight(uwe));
            }
            if (tick > 1200 && tick % 120 == 0 && mySprites.size() > 1) {
                mySprites.remove();
            }

            alternatingLeftAndRight = !alternatingLeftAndRight;
        }
        for (Sprite sprite : mySprites) {
            sprite.update();
        }
        faster+=faster;
        tick+=faster;
    }

    public Queue<Sprite> getMySprites() {
        return mySprites;
    }
}
