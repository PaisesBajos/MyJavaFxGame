package de.awacademy.model;

import java.util.LinkedList;
import java.util.Queue;

public class TeflonSpawner {
    Queue<Sprite> myTeflon;
    double tick=1;
    public TeflonSpawner(){
        myTeflon=new LinkedList<>();
    }
    public void update(){
        tick++;
        if (tick%300==0){
            myTeflon.add(new Teflon());
        }
        if (myTeflon.size()>1 && tick%300==0){
            myTeflon.remove();
        }
        for (Sprite sprite:myTeflon) {
            sprite.update();
        }
    }
public void removeLatest(){
        myTeflon.remove();
};
    public Queue<Sprite> getMyTeflon() {
        return myTeflon;
    }
}
