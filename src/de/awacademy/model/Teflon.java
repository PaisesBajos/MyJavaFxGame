package de.awacademy.model;

public class Teflon extends Sprite {
    double angle=0;
    public Teflon() {
        super();
        this.setHeight(50);
        this.setWidth(50);
        setPosition(WIDTH / 2, getHeight() / 2);
    }

    @Override
    public void update() {
            setPositionY(getPositionY()+3);
    }

    public double calculateAngle() {
        angle++;
        return angle;
    }
}
