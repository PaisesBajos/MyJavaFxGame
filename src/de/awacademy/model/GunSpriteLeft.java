package de.awacademy.model;

public class GunSpriteLeft extends Sprite {
    Uwe uwe;

    public GunSpriteLeft(Uwe uwe) {
        super();
        this.uwe = uwe;
        setPosition(WIDTH-(9.5/10.0)*WIDTH, HEIGHT -(1.0/10.0)* HEIGHT);
        this.setHeight(40);
        this.setWidth(120);

    }

    public double calculateAngle() {
        return -Math.toDegrees(Math.atan(Math.abs(HEIGHT -(1.0/10.0)* HEIGHT - uwe.getPositionY()) / Math.abs(WIDTH-(9.0/10.0)*WIDTH - uwe.getPositionX())))-10;
    }
}
