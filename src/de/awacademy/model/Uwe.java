package de.awacademy.model;
public class Uwe extends Sprite {

    int state;
    int lifes;
    double previousX = 0;
    double previousAngle=0;


    public Uwe() {
        super();
        this.setPosition(WIDTH/2, HEIGHT /2);


        this.setHeight(145*0.8);
        this.setWidth(64*0.8);
        this.lifes = 1;
    }

    public void update() {
        state = 0;
        //Gravity
        setVelocityY(getVelocityY() - 0.4*0.8);
    }
    public void updateSpace(){
        setVelocityY(getVelocityY() + 0.80*0.8);
        state = 1;
        setPositionY(getPositionY() - getVelocityY());
    }



    public int getState() {
        return state;
    }

    public int getLifes() {
        return lifes;
    }

    public void setLifes(int lifes) {
        this.lifes = lifes;
    }

    public void deminishLifes() {
        this.lifes--;
    }
    public void increaseLifes() {
        this.lifes++;
    }

    ///Rotation Functions


    public double calculateAngle() {
        double delta = getPositionX() - previousX;
        double angle = (90 * (delta/100));
        previousX = getPositionX();
        previousAngle=angle;
        return angle;

    }





}
