package de.awacademy.model;

public class GunSpriteRight extends Sprite {
    Uwe uwe;

    public GunSpriteRight(Uwe uwe) {
        super();
        this.uwe = uwe;
        setPosition(WIDTH - (1.5 / 10.0) * WIDTH, HEIGHT - (1 / 10.0) * HEIGHT);
        this.setHeight(40);
        this.setWidth(120);
    }

    public double calculateAngle() {
        return Math.toDegrees(Math.atan(Math.abs(HEIGHT - (1.0 / 10.0) * HEIGHT - uwe.getPositionY()) / Math.abs(WIDTH - (1.0 / 10.0) * WIDTH - uwe.getPositionX()))) - 10;
    }
}
