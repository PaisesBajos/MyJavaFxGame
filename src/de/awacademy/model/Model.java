package de.awacademy.model;


import java.util.ArrayList;
import java.util.List;

public class Model {

    private Uwe uwe;
    private List<Sprite> gunSprites;
    private SpriteSpawner spawny;
    private Alex alex;
    private TeflonSpawner tSpawner;

    public Model() {
        this.uwe = new Uwe();
        this.spawny = new SpriteSpawner(uwe);
        gunSprites = new ArrayList<>();
        gunSprites.add(new GunSpriteLeft(uwe));
        gunSprites.add(new GunSpriteRight(uwe));
        alex = new Alex();
        tSpawner = new TeflonSpawner();
    }

    public Uwe getUwe() {
        return uwe;
    }

    public List<Sprite> getGunSprites() {
        return gunSprites;
    }


    public SpriteSpawner getSpawny() {
        return spawny;
    }

    public Alex getAlex() {
        return alex;
    }

    public TeflonSpawner getTSpawner() {
        return tSpawner;
    }

    public void update() {
        tSpawner.update();
        alex.update();
        spawny.update();
        uwe.update();
        uwe.setPositionY(uwe.getPositionY() - uwe.getVelocityY());
    }


}
