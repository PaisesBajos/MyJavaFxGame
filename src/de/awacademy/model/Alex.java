package de.awacademy.model;

public class Alex extends Sprite{
    double angle;
    int tick;
    private boolean turning = false;
    public Alex() {
        super();
        this.angle = 0;
        this.setHeight(40);
        this.setWidth(100);
        setPosition(WIDTH/2-10,-10);
    }

    @Override
    public void update() {
        tick++;
        if (tick==300){
            turning=true;
            tick =0;
        }
        if (turning==true){
            angle+=3;
        }
        if (angle==90){
            angle = 0;
            turning =false;
        }
    }

    @Override
    public double calculateAngle() {
        return angle;
    }
}
