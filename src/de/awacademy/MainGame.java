package de.awacademy;

import de.awacademy.model.*;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class MainGame extends Application {

//

    @Override
    public void start(Stage stage) throws Exception {

        Canvas canvas = new Canvas(Sprite.WIDTH, Sprite.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);
        Scene scene = new Scene(group);

        GraphicsContext gc = canvas.getGraphicsContext2D();
        stage.setScene(scene);
        Model myModel = new Model();

        Graphics graphics = new Graphics(gc, myModel);
        stage.show();
        KeyEvents myEvents = new KeyEvents(scene, myModel, stage);
        Timer timer = new Timer(graphics, myEvents, myModel);
        //Restarter restarter = new Restarter(this,myModel,graphics,timer);
        timer.start();


    }


}
