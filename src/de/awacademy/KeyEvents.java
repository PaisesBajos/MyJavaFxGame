package de.awacademy;


import de.awacademy.model.Model;
import de.awacademy.model.Uwe;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.util.ArrayList;

public class KeyEvents {
    private ArrayList<String> input = new ArrayList<String>();
    private Uwe uwe;
    private boolean moved = false;
    private Stage stage;


    public KeyEvents(Scene theScene, Model model, Stage stage) {
        this.stage = stage;
        ////Mouse Input
        this.uwe = model.getUwe();
        theScene.setOnMouseMoved(e ->
        {
            uwe.setPositionX(e.getSceneX() - uwe.getWidth() / 2);
        });

        ////Keyboard Input
        /// Ich habs nicht über den Event Handler wie bei Mouse Events gemacht, weil ich mit der refresh rate kein gutes Ergebnis für den Boost bekommen habe
        theScene.setOnKeyPressed(e -> {
            String code = e.getCode().toString();
            if (e.getCode().toString().equals("SPACE")) {
                moved = true;
            }
            if (e.getCode().toString().equals("R")) {
                stage.close();
                Platform.runLater(() -> {
                    try {
                        new MainGame().start(new Stage());
                    } catch (Exception e1) {
                        System.out.println("Das war nix");
                    }
                });
            }

            if (!input.contains(code))
                input.add(code);
        });
        theScene.setOnKeyReleased(e -> {
                    String code = e.getCode().toString();
                    input.remove(code);
                }
        );
    }

    public ArrayList<String> getKeyEvents() {
        return this.input;
    }

    public boolean getBool() {
        return this.moved;
    }

}
