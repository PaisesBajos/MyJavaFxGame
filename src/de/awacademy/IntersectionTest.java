package de.awacademy;

import de.awacademy.model.Sprite;
import de.awacademy.model.Teflon;
import de.awacademy.model.ThrowingSpriteLeft;
import javafx.geometry.Rectangle2D;
import java.util.Queue;

public class IntersectionTest {

    IntersectionTest(){

    }
    public boolean intersects(Sprite uwe, Queue<Sprite> sprites){

        Rectangle2D uweRect = new Rectangle2D(uwe.getPositionX()+10,uwe.getPositionY()+10,uwe.getWidth()-20,uwe.getHeight()-20);
        for (Sprite sprite:sprites){
            Rectangle2D tempSprite = new Rectangle2D(sprite.getPositionX()+5,sprite.getPositionY()+5,sprite.getWidth()-10,sprite.getHeight()-10);
            if(uweRect.intersects(tempSprite)){
                if (sprite instanceof ThrowingSpriteLeft){
                    if(sprite.getVelocityY()<0){
                        sprite.setVelocityY(sprite.getVelocityY()*-4);
                        sprite.setPositionY(sprite.getPositionY()-sprite.getVelocityY());
                    }
                    sprite.setVelocityX(sprite.getVelocityX()*-4);
                    sprite.setPositionX(sprite.getPositionX()-sprite.getVelocityX());
                }
                if(sprite instanceof Teflon){
                    String path = System.getProperty("user.dir");
                    Sound sound = new Sound(path+"\\src\\de\\awacademy\\sounds\\metal.wav");
                    sound.play();
                }
                return true;
            }
        }
        return false;
    }
}
