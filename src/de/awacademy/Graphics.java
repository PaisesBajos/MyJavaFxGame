package de.awacademy;

//import de.awacademy.model.Model;

import de.awacademy.model.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;

import java.util.List;
import java.util.Queue;

public class Graphics {
    ////// Images

    String path = System.getProperty("user.dir");
    Image startImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\StartBildSchirm.png");
    Image endimage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\EndBildSchirm.png");
    Image backGroundImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\dungeonComplete.png");
    Image gunRightImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\gunRight.png");
    Image gunLeftImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\gun.png");
    Image throwingSpriteImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\trash.png");
    Image alexArmImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\armTurned.png");
    Image teflonImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\Teflon.png");
    Image uweEggBoostingImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\EggFire.png");
    Image uweEggImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\Egg.png");
    Image uweEggCrackedBoostingImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\EggCrackedFire.png");
    Image uweCrackedEggImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\EggCracked.png");
    Image uweBoostingImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\UweWithJetpackFire.png");
    Image uweNormalImage = new Image("file:"+path+"\\src\\de\\awacademy\\images\\UweJetpackWithoutFlames.png");

    private GraphicsContext gc;
    private Uwe uwe;
    private List<Sprite> gunSprites;
    private Queue<Sprite> tempSprites;
    private Alex alex;
    private Queue<Sprite> teflonSprites;
    int state = 1;

    public Graphics(GraphicsContext gc, Model myModel) {
        this.gc = gc;
        this.uwe = myModel.getUwe();
        this.gunSprites = myModel.getGunSprites();
        this.tempSprites = myModel.getSpawny().getMySprites();
        this.alex = myModel.getAlex();
        this.teflonSprites = myModel.getTSpawner().getMyTeflon();
    }

    public void draw() {
        if (state==1){
            drawStartingImage();
        }
        if (state==2){
        gc.clearRect(0, 0, Sprite.WIDTH, Sprite.HEIGHT);
        drawBackground();
        drawUwe();
        drawAlex();
        for (Sprite sprite : teflonSprites) {
            drawTeflon(sprite);
        }
        for (Sprite sprite : tempSprites) {
            drawThrowingSprite(sprite);
        }
        for (Sprite sprite : gunSprites) {
            drawGunSprite(sprite);

        }}
        if (state==3){
            drawEndImage();
        }
    }

    private void drawThrowingSprite(Sprite sprite) {

        drawRotatedImage(throwingSpriteImage, sprite.calculateAngle(), sprite.getPositionX() + sprite.getWidth() / 2, sprite.getPositionY() - sprite.getHeight() / 2, sprite.getWidth(), sprite.getHeight(), sprite);
    }

    private void drawGunSprite(Sprite sprite) {
        Image image = gunLeftImage;

        if (sprite instanceof GunSpriteRight) {
            image = gunRightImage;
        }

        drawRotatedImage(image, sprite.calculateAngle(), sprite.getPositionX() + sprite.getWidth() / 2, sprite.getPositionY() + sprite.getHeight() / 2, sprite.getWidth(), sprite.getHeight(), sprite);
    }

    private void drawTeflon(Sprite sprite) {
        drawRotatedImage(teflonImage, sprite.calculateAngle(), sprite.getPositionX() + sprite.getWidth() / 2, sprite.getPositionY() + sprite.getHeight() / 2, sprite.getWidth(), sprite.getHeight(), sprite);
    }

    private void drawUwe() {
        Image image = uweNormalImage;
        double scaleY = uwe.getHeight();
        if (uwe.getState() == 1) {

            if (uwe.getLifes() == 3) {
                image = uweEggBoostingImage;
            }
            if (uwe.getLifes() == 2) {
                image = uweEggCrackedBoostingImage;
            }
            if (uwe.getLifes() == 1) {
                image = uweBoostingImage;
            }
            scaleY = 160 * 0.8;
        } else {
            if (uwe.getLifes() == 3) {
                image = uweEggImage;
            }
            if (uwe.getLifes() == 2) {
                image = uweCrackedEggImage;
            }
            if (uwe.getLifes() == 1) {
                image = uweNormalImage;
            }
        }
        drawRotatedImage(image, uwe.calculateAngle(), uwe.getPositionX(), uwe.getPositionY(), uwe.getWidth(), scaleY, uwe);
    }

    private void drawAlex() {
        drawRotatedImage(alexArmImage, alex.calculateAngle(), alex.getPositionX(), 0, alex.getWidth(), alex.getHeight(), alex);

    }

    public void setState(int state) {
        this.state = state;
    }

    protected void drawRotatedImage(Image image, double angle, double tlpx, double tlpy, double scaleX, double scaleY, Sprite sprite) {
        gc.save();
        rotate(angle, tlpx, tlpy);
        gc.drawImage(image, sprite.getPositionX(), sprite.getPositionY(), scaleX, scaleY);
        gc.restore();
    }

    public void drawStartingImage() {
        gc.save();
        rotate(0, 0, 0);
        gc.drawImage(startImage, 0, 0,Sprite.WIDTH, Sprite.HEIGHT);
        gc.restore();
    }
    public void drawEndImage() {
        String path = System.getProperty("user.dir");
        Sound sound = new Sound(path+"\\src\\de.awacademy\\sounds\\fatality.wav");
        sound.play();
        gc.save();
        rotate(0, 0, 0);
        gc.drawImage(endimage, 0, 0,Sprite.WIDTH, Sprite.HEIGHT);
        gc.restore();
    }
    public void drawBackground() {
        gc.save();
        rotate(0, 0, 0);
        gc.drawImage(backGroundImage, 0, 0,Sprite.WIDTH, Sprite.HEIGHT);
        gc.restore();
    }
    protected void rotate(double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }


}
